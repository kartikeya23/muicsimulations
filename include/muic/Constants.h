//
// Created by omiguelc on 4/6/22.
//

#ifndef MUICSIM_CONSTANTS_H
#define MUICSIM_CONSTANTS_H

// Machines
std::map<std::string, std::map<string, std::any>> machines = {
    {"MuIC",
     {{"output", "muic"},
      {"note", "For MuIC analysis"},
      {"eProton", 275.}, // GeV
      {"eMuon", 960.}}},
    {"MuIC2",
     {{"output", "muic2"},
      {"note", "For MuIC analysis"},
      {"eProton", 1000.}, // GeV
      {"eMuon", 1000.}}},
    {"LHmuC",
     {{"output", "LHmuC"},
      {"note", "For MuIC analysis"},
      {"eProton", 7000.}, // GeV
      {"eMuon", 1500.}}},
    {"HERA",
     {{"output", "muic_hera"},
      {"note", "For comparing with HERA"},
      {"eProton", 920.}, // GeV
      {"eMuon", 27.5}}},
};

// FIDUCIAL CUTS
std::map<std::string, std::map<string, std::any>> fiducial_cut_sets = {
    {"nocuts",
     {{"output", ""},
      {"note", "No cuts"},
      {"Q2Min", 0}, // GeV
      {"yMin", 0},
      {"yMax", 1}}},
    {"legacy",
     {{"output", "q21_ymin0p001_ymax0p999"},
      {"note", "Used by Dr. Wei Li"},
      {"Q2Min", 1.}, // GeV
      {"yMin", 0.001},
      {"yMax", 0.999}}},
    {"main",
     {{"output", "q21_ymin0p01_ymax0p99"},
      {"note", "For main analysis"},
      {"Q2Min", 1.}, // GeV
      {"yMin", 0.01},
      {"yMax", 0.99}}},
    {"hera",
     {{"output", "q21_ymin0p01_ymax0p9"},
      {"note", "For hera comparison"},
      {"Q2Min", 1.}, // GeV
      {"yMin", 0.01},
      {"yMax", 0.9}}},
    {"heraQ2_100",
     {{"output", "q2100_ymin0p01_ymax0p9"},
      {"note", "For hera comparison @ Q2 > 100 GeV^2"},
      {"Q2Min", 100.}, // GeV
      {"yMin", 0.01},
      {"yMax", 0.9}}},
    {"heraQ2_2k",
     {{"output", "q22k_ymin0p01_ymax0p9"},
      {"note", "For hera comparison @ Q2 > 2k GeV^2"},
      {"Q2Min", 2e3}, // GeV
      {"yMin", 0.01},
      {"yMax", 0.9}}},
    {"heraQ2_20k",
     {{"output", "q220k_ymin0p01_ymax0p9"},
      {"note", "For hera comparison @ Q2 > 20k GeV^2"},
      {"Q2Min", 20e3}, // GeV
      {"yMin", 0.01},
      {"yMax", 0.9}}},
    {"heraQ2_200k",
     {{"output", "q2200k_ymin0p01_ymax0p9"},
      {"note", "For hera comparison @ Q2 > 200k GeV^2"},
      {"Q2Min", 200e3}, // GeV
      {"yMin", 0.01},
      {"yMax", 0.9}}},
    {"heraQ2_2M",
     {{"output", "q22M_ymin0p01_ymax0p9"},
      {"note", "For hera comparison @ Q2 > 2M GeV^2"},
      {"Q2Min", 2e6}, // GeV
      {"yMin", 0.01},
      {"yMax", 0.9}}},
};

#endif // MUICSIM_CONSTANTS_H
