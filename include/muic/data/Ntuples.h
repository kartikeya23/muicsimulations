//
// Created by omiguelc on 09/01/22.
//

#ifndef MUICSIM_NTUPLES_H
#define MUICSIM_NTUPLES_H

#include <vector>

#include "TTree.h"

using namespace std;

class OutputNtuple {
  public:
    OutputNtuple();

    ~OutputNtuple();

    double evt_weight;

    double Q2, W2, x, y, mu_pT, mu_eta;

    double rec_Q2, rec_x, rec_y, rec_agljb, recsm_Q2, recsm_x, recsm_y;

    int vsize_had;
    vector<int> had_id;
    vector<double> had_px, had_py, had_pz, had_e, had_phi, had_eta, had_mass;

    int vsize_recg;
    vector<string> recg_name;
    vector<int> recg_had_n;
    vector<double> recg_sum_e, recg_sum_px, recg_sum_py, recg_sum_pz;
    vector<double> recg_agljb;
    vector<double> recg_Q2jb, recg_xjb, recg_yjb;
    vector<double> recg_Q2DA, recg_xDA, recg_yDA;
    vector<double> recg_Q2A4, recg_xA4, recg_yA4;

    void registerBranches(TTree *tree);

    void clear();
};

#endif // MUICSIM_NTUPLES_H
