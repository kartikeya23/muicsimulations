//
// Created by omiguelc on 08/02/22.
//

#ifndef MUICSIM_PYTHIAGENERATOR_H
#define MUICSIM_PYTHIAGENERATOR_H

#include "muic/analysis/KinematicReconstruction.h"

typedef struct {
    KinematicReconstruction *reconstruction;
    std::function<bool(const Particle &particle)> condition;
} HadronReconstructionGroup;

class PythiaGenerator {

  private:
    std::string output_path;

    int n_events;
    string lhe_input_file_path;
    double e_proton; // GeV
    double e_muon;   // GeV
    double Q2_min;   // GeV^2
    double y_min;
    double y_max;
    bool use_antimuon_en;
    bool use_charged_current_en;
    bool use_neutral_current_en;

  public:
    PythiaGenerator();

    ~PythiaGenerator() = default;

    const string &getOutputPath() const;

    void setOutputPath(const string &outputName);

    int getNEvents() const;

    void setNEvents(int nEvents);

    string getLHEInputFile() const;

    void setLHEInputFile(string file_path);

    double getEProton() const;

    void setEProton(double eProton);

    double getEMuon() const;

    void setEMuon(double eMuon);

    double getQ2Min() const;

    void setQ2Min(double q2Min);

    double getYMin() const;

    void setYMin(double yMin);

    double getYMax() const;

    void setYMax(double yMax);

    bool isUseAntimuonEn() const;

    void setUseAntimuonEn(bool useAntimuonEn);

    bool isUseChargedCurrentEn() const;

    void setUseChargedCurrentEn(bool useChargedCurrentEn);

    bool isUseNeutralCurrentEn() const;

    void setUseNeutralCurrentEn(bool useNeutralCurrentEn);

    void run();
};

#endif // MUICSIM_PYTHIAGENERATOR_H
