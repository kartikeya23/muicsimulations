//
// Created by omiguelc on 13/01/22.
//

#ifndef MUICSIM_KINEMATICRECONSTRUCTION_H
#define MUICSIM_KINEMATICRECONSTRUCTION_H

#include "Pythia8/Pythia.h"

using namespace Pythia8;

class KinematicReconstruction {

  private:
    int n;
    double sumE, sumPx, sumPy, sumPz, sumPT, sumEPz;

  public:
    KinematicReconstruction();

    ~KinematicReconstruction() = default;

    int getN() const;
    double getSumE() const;
    double getSumPx() const;
    double getSumPy() const;
    double getSumPz() const;

    void addParticle(const double &pe, const double &ppx, const double &ppy,
                     const double &ppz);

    std::tuple<double, double, double, double, double, double, double, double,
               double, double>
    reconstruct(const Vec4 &pmuIn, const double &muThetaSmear,
                const double &muESmear, const double &s) const;
};

#endif // MUICSIM_KINEMATICRECONSTRUCTION_H
