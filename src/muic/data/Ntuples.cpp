//
// Created by omiguelc on 09/01/22.
//

#include "muic/data/Ntuples.h"

OutputNtuple::OutputNtuple() { clear(); }

OutputNtuple::~OutputNtuple() = default;

void OutputNtuple::registerBranches(TTree *tree) {
    tree->Branch("evt_weight", &evt_weight);

    tree->Branch("Q2", &Q2);
    tree->Branch("W2", &W2);
    tree->Branch("x", &x);
    tree->Branch("y", &y);
    tree->Branch("mu_pT", &mu_pT);
    tree->Branch("mu_eta", &mu_eta);
    tree->Branch("rec_Q2", &rec_Q2);
    tree->Branch("rec_x", &rec_x);
    tree->Branch("rec_y", &rec_y);
    tree->Branch("rec_agljb", &rec_agljb);
    tree->Branch("recsm_Q2", &recsm_Q2);
    tree->Branch("recsm_x", &recsm_x);
    tree->Branch("recsm_y", &recsm_y);

    tree->Branch("vsize_had", &vsize_had);
    tree->Branch("had_id", &had_id);
    tree->Branch("had_px", &had_px);
    tree->Branch("had_py", &had_py);
    tree->Branch("had_pz", &had_pz);
    tree->Branch("had_e", &had_e);
    tree->Branch("had_phi", &had_phi);
    tree->Branch("had_eta", &had_eta);
    tree->Branch("had_mass", &had_mass);

    tree->Branch("vsize_recg", &vsize_recg);
    tree->Branch("recg_name", &recg_name);
    tree->Branch("recg_had_n", &recg_had_n);
    tree->Branch("recg_sum_e", &recg_sum_e);
    tree->Branch("recg_sum_px", &recg_sum_px);
    tree->Branch("recg_sum_py", &recg_sum_py);
    tree->Branch("recg_sum_pz", &recg_sum_pz);
    tree->Branch("recg_agljb", &recg_agljb);
    tree->Branch("recg_Q2jb", &recg_Q2jb);
    tree->Branch("recg_xjb", &recg_xjb);
    tree->Branch("recg_yjb", &recg_yjb);
    tree->Branch("recg_Q2DA", &recg_Q2DA);
    tree->Branch("recg_xDA", &recg_xDA);
    tree->Branch("recg_yDA", &recg_yDA);
    tree->Branch("recg_Q2A4", &recg_Q2A4);
    tree->Branch("recg_xA4", &recg_xA4);
    tree->Branch("recg_yA4", &recg_yA4);
}

void OutputNtuple::clear() {
    evt_weight = 0.;

    Q2 = -1;
    W2 = -1;
    x = -1;
    y = -1;
    mu_pT = -1;
    mu_eta = -1;
    rec_Q2 = -1;
    rec_x = -1;
    rec_y = -1;
    rec_agljb = -1;
    recsm_Q2 = -1;
    recsm_x = -1;
    recsm_y = -1;

    vsize_had = 0;
    had_id.clear();
    had_px.clear();
    had_py.clear();
    had_pz.clear();
    had_e.clear();
    had_phi.clear();
    had_eta.clear();
    had_mass.clear();

    vsize_recg = 0;
    recg_name.clear();
    recg_had_n.clear();
    recg_sum_e.clear();
    recg_sum_px.clear();
    recg_sum_py.clear();
    recg_sum_pz.clear();
    recg_agljb.clear();
    recg_Q2jb.clear();
    recg_xjb.clear();
    recg_yjb.clear();
    recg_Q2DA.clear();
    recg_xDA.clear();
    recg_yDA.clear();
    recg_Q2A4.clear();
    recg_xA4.clear();
    recg_yA4.clear();
}
