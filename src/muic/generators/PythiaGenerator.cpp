//
// Created by omiguelc on 08/02/22.
//

#include "muic/generators/PythiaGenerator.h"

#include <any>

#include "Pythia8/Pythia.h"
#include "boost/algorithm/string_regex.hpp"
#include "boost/format.hpp"

#include "TF1.h"
#include "TFile.h"
#include "TH1.h"
#include "TRandom3.h"
#include "TTree.h"

#include "muic/analysis/KinematicReconstruction.h"
#include "muic/data/Ntuples.h"

using namespace Pythia8;

PythiaGenerator::PythiaGenerator() {
    output_path = "default.root";
    n_events = 50000;
    lhe_input_file_path = "";
    e_proton = 275; // GeV
    e_muon = 960;   // GeV
    Q2_min = 1;     // GeV^2
    y_min = 0.01;
    y_max = 0.99;
    use_charged_current_en = true;
    use_neutral_current_en = false;
    use_antimuon_en = false;
}

const string &PythiaGenerator::getOutputPath() const { return output_path; }

void PythiaGenerator::setOutputPath(const string &outputPath) {
    output_path = outputPath;
}

int PythiaGenerator::getNEvents() const { return n_events; }

void PythiaGenerator::setNEvents(int nEvents) { n_events = nEvents; }

string PythiaGenerator::getLHEInputFile() const { return lhe_input_file_path; }

void PythiaGenerator::setLHEInputFile(string file_path) {
    lhe_input_file_path = file_path;
}

double PythiaGenerator::getEProton() const { return e_proton; }

void PythiaGenerator::setEProton(double eProton) { e_proton = eProton; }

double PythiaGenerator::getEMuon() const { return e_muon; }

void PythiaGenerator::setEMuon(double eMuon) { e_muon = eMuon; }

double PythiaGenerator::getQ2Min() const { return Q2_min; }

void PythiaGenerator::setQ2Min(double q2Min) { Q2_min = q2Min; }

double PythiaGenerator::getYMin() const { return y_min; }

void PythiaGenerator::setYMin(double yMin) { y_min = yMin; }

double PythiaGenerator::getYMax() const { return y_max; }

void PythiaGenerator::setYMax(double yMax) { y_max = yMax; }

bool PythiaGenerator::isUseAntimuonEn() const { return use_antimuon_en; }

void PythiaGenerator::setUseAntimuonEn(bool useAntimuonEn) {
    use_antimuon_en = useAntimuonEn;
}

bool PythiaGenerator::isUseChargedCurrentEn() const {
    return use_charged_current_en;
}

void PythiaGenerator::setUseChargedCurrentEn(bool useChargedCurrentEn) {
    use_charged_current_en = useChargedCurrentEn;
}

bool PythiaGenerator::isUseNeutralCurrentEn() const {
    return use_neutral_current_en;
}

void PythiaGenerator::setUseNeutralCurrentEn(bool useNeutralCurrentEn) {
    use_neutral_current_en = useNeutralCurrentEn;
}

void PythiaGenerator::run() {
    // Muon ID
    int muon_id = 13;

    if (use_antimuon_en) {
        muon_id = -muon_id;
    }

    // Calculate s
    double m_proton = 0.938272; // GeV/c^2
    double p_proton = sqrt(pow(e_proton, 2) - pow(m_proton, 2));

    double m_muon = 0.105658; // GeV/c^2
    double p_muon = sqrt(pow(e_muon, 2) - pow(m_muon, 2));

    double s = pow(e_muon + e_proton, 2) - pow(p_muon - p_proton, 2);

    // Resolution functions
    double muAngleRes = 0.0002;
    auto *muAngleResFunc = new TF1("muAngleResFunc", "exp(-0.5*x*x/[0]/[0])",
                                   -10. * muAngleRes, 10. * muAngleRes);
    muAngleResFunc->SetParameter(0, muAngleRes);

    double muPRelRes = 0.01;
    auto *muPRelResFunc =
        new TF1("muPRelResFunc",
                "exp(-0.5*x*x/([0]*[0]+0.0001*0.0001*[1]*[1]))", -10, 10);
    muPRelResFunc->SetParameter(0, muPRelRes);

    double chTrkPRelRes = 0.01;
    auto *chTrkPRelResFunc =
        new TF1("chTrkPRelResFunc",
                "exp(-0.5*x*x/([0]*[0]+0.001*0.001*[1]*[1]))", -10, 10);
    chTrkPRelResFunc->SetParameter(0, chTrkPRelRes);

    double chTrkAngleRes = 0.0002;
    auto *chTrkAngleResFunc =
        new TF1("chTrkAngleResFunc",
                "exp(-0.5*x*x/([0]*[0]+0.002*0.002/[1]/[1]))", -0.01, 0.01);
    chTrkAngleResFunc->SetParameter(0, chTrkAngleRes);

    double caloAngleRes = 0.087 / sqrt(12);
    auto *caloAngleResFunc =
        new TF1("caloAngleResFunc", "exp(-0.5*x*x/[0]/[0])",
                -10. * caloAngleRes, 10. * caloAngleRes);
    caloAngleResFunc->SetParameter(0, caloAngleRes);

    double emERelRes = 0.02;
    auto *emERelResFunc =
        new TF1("emERelResFunc", "exp(-0.5*x*x/([0]*[0]+0.01/[1]))", -10, 10);
    emERelResFunc->SetParameter(0, emERelRes);

    double hadERelRes = 0.1;
    auto *hadERelResFunc =
        new TF1("hadERelResFunc", "exp(-0.5*x*x/([0]*[0]+0.25/[1]))", -10, 10);
    hadERelResFunc->SetParameter(0, hadERelRes);

    // Generator. Shorthand for event.
    Pythia pythia;
    auto &event = pythia.event;

    if (lhe_input_file_path == "") {
        // Set up incoming beams, for frame with unequal beam energies.
        pythia.readString("Beams:frameType = 2");

        // BeamA = proton.
        pythia.readString("Beams:idA = 2212");
        pythia.settings.parm("Beams:eA", e_proton);

        // BeamB = muon.
        pythia.readString("Beams:idB = " + std::to_string(muon_id));
        pythia.settings.parm("Beams:eB", e_muon);

        // Set up DIS process within some phase space.
        // Neutral current: Scattering f f' → f f' via gamma^*/Z^0 t-channel
        // exchange, with full interference between the gamma^* and Z^0
        if (use_neutral_current_en) {
            pythia.readString("WeakBosonExchange:ff2ff(t:gmZ) = on");
        }

        // Charged current: Scattering f_1 f_2 → f_3 f_4 via W^+- t-channel
        // exchange
        if (use_charged_current_en) {
            pythia.readString("WeakBosonExchange:ff2ff(t:W) = on");
        }
    } else {
        pythia.readString("Beams:frameType = 4");
        pythia.readString("Beams:LHEF = " + lhe_input_file_path);
        pythia.readString("JetMatching:setMad=off");
        pythia.readString("BeamRemnants:remnantMode=1");
        pythia.readString("ColourReconnection:mode=1");
    }

    // Phase-space cut: minimal Q2 of process.
    pythia.settings.parm("PhaseSpace:Q2Min", Q2_min);

    // Set dipole recoil on. Necessary for DIS + shower.
    pythia.readString("SpaceShower:dipoleRecoil = on");

    // Allow emissions up to the kinematical limit,
    // since rate known to match well to matrix elements everywhere.
    pythia.readString("SpaceShower:pTmaxMatch = 2");

    // QED radiation off lepton not handled yet by the new procedure.
    pythia.readString("PDF:lepton = off");
    pythia.readString("TimeShower:QEDshowerByL = off");

    // Initialize.
    pythia.init();

    // Set up the ROOT TFile and TTree.
    auto *file = TFile::Open(output_path.c_str(), "recreate");

    auto *meta_tree = new TTree("meta", "Metadata Tree");

    int events_total = 0, events_processed = 0;
    double events_total_weighted = 0, events_processed_weighted = 0;

    meta_tree->Branch("events_total", &events_total);
    meta_tree->Branch("events_processed", &events_processed);
    meta_tree->Branch("events_total_weighted", &events_total_weighted);
    meta_tree->Branch("events_processed_weighted", &events_processed_weighted);

    auto *event_tree = new TTree("evt", "Event Tree");
    auto *output_ntuple = new OutputNtuple();
    output_ntuple->registerBranches(event_tree);

    // Begin event loop.
    while (pythia.next() && events_processed < n_events) {
        // CLEAR NTUPLE FOR NEXT EVENT
        output_ntuple->clear();

        // GET EVENT INFORMATION
        double event_weight = pythia.info.weight();

        // INCREASE EVENT TOTAL
        events_total++;
        events_total_weighted += event_weight;

        // Four-momenta of proton, muon, virtual photon/Z^0/W^+-.
        auto &proton_in = event[1];
        auto &muon_in = event[4];
        auto &muon_out = event[6];

        auto p_proton_in = proton_in.p();
        auto p_muon_in = muon_in.p();
        auto p_muon_out = muon_out.p();
        auto p_virtual = p_muon_in - p_muon_out;

        // CALCULATE Q2, W2, Bjorken x, y.
        double Q2 = -p_virtual.m2Calc();
        double W2 = (p_proton_in + p_virtual).m2Calc();
        double x = Q2 / (2. * p_proton_in * p_virtual);
        double y = (p_proton_in * p_virtual) / (p_proton_in * p_muon_in);

        // CALCULATE Q2, W2, Bjorken x, y MUON METHOD
        double rec_Q2 = 4. * p_muon_in.e() * p_muon_out.e() *
                        pow(cos(p_muon_out.theta() / 2.), 2);
        double rec_y = 1 - p_muon_out.e() * (1 - cos(p_muon_out.theta())) /
                               (2. * p_muon_in.e());
        double rec_x = rec_Q2 / (s * rec_y);

        double rec_agljb =
            2 * atan(1 / sqrt(4 * pow(p_proton_in.e(), 2) * pow(x, 2) / Q2 -
                              x * p_proton_in.e() / p_muon_in.e()));

        // SHORT-CIRCUIT
        if (rec_y < y_min || y_max < rec_y) {
            continue;
        }

        // INCREASE EVENTS PROCESSED
        events_processed++;
        events_processed_weighted += event_weight;

        // STORE EVENT INFORMATION
        output_ntuple->evt_weight = event_weight;

        // SMEARED Q2, W2, Bjorken x, y
        muPRelResFunc->SetParameter(1, p_muon_out.pAbs());
        muAngleResFunc->SetParameter(1, p_muon_out.pAbs());

        double muon_theta_smear =
            p_muon_out.theta() + muAngleResFunc->GetRandom();
        double muon_p_smear =
            p_muon_out.pAbs() * (1 + muPRelResFunc->GetRandom());
        double muon_e_smear = sqrt(pow(muon_p_smear, 2) + pow(m_muon, 2));

        double recsm_Q2 =
            2 * p_muon_in.e() * muon_e_smear * (1 + cos(muon_theta_smear));
        double recsm_y = 1 - muon_e_smear / (2. * p_muon_in.e()) *
                                 (1 - cos(muon_theta_smear));
        double recsm_x = recsm_Q2 / (s * recsm_y);

        // Fill kinematics histograms.
        output_ntuple->Q2 = Q2;
        output_ntuple->W2 = W2;
        output_ntuple->x = x;
        output_ntuple->y = y;
        output_ntuple->mu_pT = muon_out.pT();
        output_ntuple->mu_eta = muon_out.eta();

        output_ntuple->rec_Q2 = rec_Q2;
        output_ntuple->rec_x = rec_x;
        output_ntuple->rec_y = rec_y;
        output_ntuple->rec_agljb = rec_agljb;
        output_ntuple->recsm_Q2 = recsm_Q2;
        output_ntuple->recsm_x = recsm_x;
        output_ntuple->recsm_y = recsm_y;

        // Setup Hadron Reconstructions
        std::map<std::string, HadronReconstructionGroup> reconstruction_groups;

        // Add Nocuts
        reconstruction_groups["nocuts"] = {
            new KinematicReconstruction(),
            [](const Particle &particle) { return true; }};

        // Define Eta Vars
        double min_eta = -4.5, max_eta = 1.5, step_eta = 1.;

        // Add Stepped Eta Cuts
        double low_eta = min_eta, up_eta = min_eta + step_eta;

        while (up_eta <= max_eta) {
            stringstream reconstruction_name_ss;

            reconstruction_name_ss << "eta"
                                   << boost::format("_gt%0.1f") % low_eta
                                   << boost::format("_lt%0.1f") % up_eta;

            std::string reconstruction_name = reconstruction_name_ss.str();
            boost::replace_all(reconstruction_name, ".", "p");
            boost::replace_all(reconstruction_name, "-", "m");

            reconstruction_groups[reconstruction_name] = {
                new KinematicReconstruction(),
                [low_eta, up_eta](const Particle &particle) {
                    return low_eta < particle.eta() && particle.eta() < up_eta;
                }};

            low_eta += step_eta;
            up_eta += step_eta;
        }

        // Add Standard Cuts
        reconstruction_groups["eta_gtm5p0_lt2p4"] = {
            new KinematicReconstruction(), [](const Particle &particle) {
                return -5. < particle.eta() && particle.eta() < 2.4;
            }};

        reconstruction_groups["eta_gtm4p0_lt2p4"] = {
            new KinematicReconstruction(), [](const Particle &particle) {
                return -4. < particle.eta() && particle.eta() < 2.4;
            }};

        reconstruction_groups["eta_2p4"] = {
            new KinematicReconstruction(), [](const Particle &particle) {
                return fabs(particle.eta()) < 2.4;
            }};

        reconstruction_groups["eta_3"] = {
            new KinematicReconstruction(),
            [](const Particle &particle) { return fabs(particle.eta()) < 3; }};

        reconstruction_groups["eta_4"] = {
            new KinematicReconstruction(),
            [](const Particle &particle) { return fabs(particle.eta()) < 4; }};

        reconstruction_groups["eta_5"] = {
            new KinematicReconstruction(),
            [](const Particle &particle) { return fabs(particle.eta()) < 5; }};

        if (events_processed == 0) {
            for (const auto &[name, reconstruction] : reconstruction_groups) {
                std::cout << "Registered Reconstruction: " << name << std::endl;
            }
        }

        // Loop particles
        int particle_vsize = 0;

        for (int i = 0; i < event.size(); ++i) {
            // Short-Circuit: Skip all particles which aren't final,
            // and the original muon
            if (!event[i].isFinal() || event[i].id() == muon_id) {
                continue;
            }

            // Reconstruct
            int particle_id = event[i].id();
            double particle_p = event[i].pAbs();
            double particle_px = event[i].px();
            double particle_py = event[i].py();
            double particle_pz = event[i].pz();
            double particle_e = event[i].e();
            double particle_phi = event[i].phi();
            double particle_eta = event[i].eta();
            double particle_mass = event[i].m();

            // Store kinematics of final state
            output_ntuple->had_id.push_back(particle_id);
            output_ntuple->had_px.push_back(particle_px);
            output_ntuple->had_py.push_back(particle_py);
            output_ntuple->had_pz.push_back(particle_pz);
            output_ntuple->had_e.push_back(particle_e);
            output_ntuple->had_phi.push_back(particle_phi);
            output_ntuple->had_eta.push_back(particle_eta);
            output_ntuple->had_mass.push_back(particle_mass);

            // charged particles
            if (fabs(particle_id) == 211 || fabs(particle_id) == 321 ||
                fabs(particle_id) == 2212 || fabs(particle_id) == 11) {
                chTrkPRelResFunc->SetParameter(1, particle_p);

                particle_p *= (1 + chTrkPRelResFunc->GetRandom());
                particle_phi += chTrkAngleResFunc->GetRandom();
                particle_eta += chTrkAngleResFunc->GetRandom();
                particle_px =
                    particle_p / cosh(particle_eta) * cos(particle_phi);
                particle_py =
                    particle_p / cosh(particle_eta) * sin(particle_phi);
                particle_pz =
                    particle_p / cosh(particle_eta) * sinh(particle_eta);
                particle_e = sqrt(
                    particle_mass * particle_mass + particle_px * particle_px +
                    particle_py * particle_py + particle_pz * particle_pz);
            }

            // photons
            if (fabs(particle_id) == 22) {
                emERelResFunc->SetParameter(1, particle_e);

                particle_e *= (1 + emERelResFunc->GetRandom());
                particle_phi += caloAngleResFunc->GetRandom();
                particle_eta += caloAngleResFunc->GetRandom();
                particle_px =
                    particle_e / cosh(particle_eta) * cos(particle_phi);
                particle_py =
                    particle_e / cosh(particle_eta) * sin(particle_phi);
                particle_pz =
                    particle_e / cosh(particle_eta) * sinh(particle_eta);
            }

            // neutrons
            if (fabs(particle_id) == 2112) {
                hadERelResFunc->SetParameter(1, particle_e);

                particle_e *= (1 + hadERelResFunc->GetRandom());
                particle_phi += caloAngleResFunc->GetRandom();
                particle_eta += caloAngleResFunc->GetRandom();
                particle_px =
                    particle_e / cosh(particle_eta) * cos(particle_phi);
                particle_py =
                    particle_e / cosh(particle_eta) * sin(particle_phi);
                particle_pz =
                    particle_e / cosh(particle_eta) * sinh(particle_eta);
            }

            // Adding to reconstructions
            for (auto &[name, rgroup] : reconstruction_groups) {
                // Short-Circuit
                if (!rgroup.condition(event[i])) {
                    continue;
                }

                // Add particle
                rgroup.reconstruction->addParticle(particle_e, particle_px,
                                                   particle_py, particle_pz);
            }

            // INCREASE COUNTER
            particle_vsize++;
        }

        output_ntuple->vsize_had = particle_vsize;
        // End Loop particles

        // Store cuts
        int vsize_recg = 0;

        for (const auto &[name, rgroup] : reconstruction_groups) {
            // Reconstruct
            const auto &result = rgroup.reconstruction->reconstruct(
                p_muon_in, muon_theta_smear, muon_e_smear, s);

            // Append to ntuple
            if (rgroup.reconstruction->getN() > 0) {
                output_ntuple->recg_name.push_back(name);
                output_ntuple->recg_had_n.push_back(
                    rgroup.reconstruction->getN());
                output_ntuple->recg_sum_e.push_back(
                    rgroup.reconstruction->getSumE());
                output_ntuple->recg_sum_px.push_back(
                    rgroup.reconstruction->getSumPx());
                output_ntuple->recg_sum_py.push_back(
                    rgroup.reconstruction->getSumPy());
                output_ntuple->recg_sum_pz.push_back(
                    rgroup.reconstruction->getSumPz());
                output_ntuple->recg_agljb.push_back(get<0>(result));
                output_ntuple->recg_Q2jb.push_back(get<1>(result));
                output_ntuple->recg_xjb.push_back(get<2>(result));
                output_ntuple->recg_yjb.push_back(get<3>(result));
                output_ntuple->recg_Q2DA.push_back(get<4>(result));
                output_ntuple->recg_xDA.push_back(get<5>(result));
                output_ntuple->recg_yDA.push_back(get<6>(result));
                output_ntuple->recg_Q2A4.push_back(get<7>(result));
                output_ntuple->recg_xA4.push_back(get<8>(result));
                output_ntuple->recg_yA4.push_back(get<9>(result));

                // Increase the number of reconstructions by 1
                vsize_recg++;
            }

            delete rgroup.reconstruction;
        }

        output_ntuple->vsize_recg = vsize_recg;

        event_tree->Fill();
    }
    // End of event loop. Statistics and histograms.

    // Print Stats
    pythia.stat();

    // The code can be obtained by looking at the stat output
    // here we will dump all of them 211
    int nProcesses = 0;
    float sigma = pythia.info.sigmaGen(0);
    float sigma_error = pythia.info.sigmaErr(0);
    vector<std::string> processes;
    vector<double> process_sigma, process_sigma_error;
    double weight_sum = 0;

    for (const auto &[i, name] : pythia.info.procNameM) {
        nProcesses++;
        processes.push_back(name);
        process_sigma.push_back(pythia.info.sigmaGen(i));
        process_sigma_error.push_back(pythia.info.sigmaErr(i));
    }

    weight_sum = pythia.info.weightSum();

    meta_tree->Branch("vsize_proc", &nProcesses);
    meta_tree->Branch("proc", &processes);
    meta_tree->Branch("proc_sigma", &process_sigma);             // mb
    meta_tree->Branch("proc_sigma_error", &process_sigma_error); // mb
    meta_tree->Branch("sigma", &sigma);                          // mb
    meta_tree->Branch("sigma_error", &sigma_error);              // mb
    meta_tree->Branch("weight_sum", &weight_sum);
    meta_tree->Fill();

    //  Write trees
    meta_tree->Write();
    event_tree->Write();

    // Close file
    file->Close();

    // Clear memory
    delete file;
    delete output_ntuple;
}
