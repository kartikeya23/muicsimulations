#include "ROOT/TProcessExecutor.hxx"
#include "muic/generators/PythiaGenerator.h"
#include <any>
#include <muic/Constants.h>
#include <thread>

using namespace Pythia8;

typedef struct {
    int run_number;
    int n_events;
    std::string output_dir;
    std::string output_ext;
    std::map<string, std::any> machine;
    std::map<string, std::any> variation;
    std::map<string, std::any> fc_set;
} DISRunArgs;

typedef struct {
    int run_number;
    int n_events;
    std::string output_dir;
    std::string output_ext;
    std::map<string, std::any> machine;
    std::map<string, std::any> fc_set;
    std::string input_lhef;
    std::string output_name;
} DISLHERunArgs;

typedef struct {
    std::atomic_int total;
    std::atomic_int ongoing;
    std::atomic_int done;
} RunStats;

// FORWARD DECLARATIONS BECAUSE CPP IS DUMB
void runDIS();
void runDISFromLHE();

// MAIN
int main() {
    // runDIS();
    runDISFromLHE();

    // RETURN
    return 0;
}

// METHODS
void runDIS() {
    // Run Options
    // User should specify the datasets they want to generate here.
    int n_events = 50000;
    std::string output_dir = "../out/";
    std::string output_ext = "_50k";
    std::vector<string> run_machines = {"MuIC"};
    std::vector<string> run_variations = {"NCMu-"};
    std::vector<string> run_fiducial_cut_sets = {
        "hera", "heraQ2_100", "heraQ2_2k", "heraQ2_20k", "heraQ2_200k"};

    // Variations
    std::map<std::string, std::map<string, std::any>> variations = {
        {"NCMu-",
         {{"output", "nc_mu"},
          {"note", "Neutral current mu-"},
          {"nc", true},  // GeV
          {"cc", false}, // GeV
          {"anti", false}}},
        {"NCMu+",
         {{"output", "nc_anti_mu"},
          {"note", "Neutral current mu+"},
          {"nc", true},  // GeV
          {"cc", false}, // GeV
          {"anti", true}}},
        {"CCMu-",
         {{"output", "cc_mu"},
          {"note", "Charged current mu-"},
          {"nc", false}, // GeV
          {"cc", true},  // GeV
          {"anti", false}}},
        {"CCMu+",
         {{"output", "cc_anti_mu"},
          {"note", "Charged current mu+"},
          {"nc", false}, // GeV
          {"cc", true},  // GeV
          {"anti", true}}},
    };

    // Create the pool of workers
    auto hardware_concurrency = std::thread::hardware_concurrency();

    std::cout << "Cores " << hardware_concurrency << " that wil go brrrr."
              << std::endl;

    ROOT::TProcessExecutor workers(hardware_concurrency);

    // Build run args
    int job_number = 0;

    std::vector<DISRunArgs> run_args;

    for (auto &variation_name : run_variations) {
        for (auto &machine_name : run_machines) {
            for (auto &limit_name : run_fiducial_cut_sets) {
                run_args.push_back({job_number++, n_events, output_dir,
                                    output_ext, machines[machine_name],
                                    variations[variation_name],
                                    fiducial_cut_sets[limit_name]});
            }
        }
    }

    std::cout << "Generating " << run_args.size() << " datasets." << std::endl;

    // Define job function
    RunStats run_stats;
    run_stats.total = run_args.size();
    run_stats.ongoing = 0;
    run_stats.done = 0;

    auto run_function = [&run_stats](DISRunArgs run_args) {
        // Update Counters
        run_stats.ongoing++;

        // Get parameters
        auto &run_number = run_args.run_number;
        auto &n_events = run_args.n_events;
        auto &output_dir = run_args.output_dir;
        auto &output_ext = run_args.output_ext;
        auto &machine = run_args.machine;
        auto &variation = run_args.variation;
        auto &fc_set = run_args.fc_set;

        // Build output name
        std::string output_machine_name =
            std::any_cast<const char *>(machine["output"]);
        std::string output_variation_name =
            std::any_cast<const char *>(variation["output"]);
        std::string output_limit_name =
            std::any_cast<const char *>(fc_set["output"]);
        std::string output_name = output_machine_name + "_" +
                                  output_variation_name + "_" +
                                  output_limit_name + output_ext + ".root";

        // Log
        std::cout << "Run " << run_number << " begin." << std::endl;
        std::cout << "Runs Remaining To Process: "
                  << (run_stats.total - run_stats.done - run_stats.ongoing)
                  << " end." << std::endl;
        std::cout << "Run " << run_number << " Output: " << output_name
                  << std::endl;

        // Configure
        PythiaGenerator generator;
        generator.setOutputPath(output_dir + output_name);
        generator.setNEvents(n_events);
        generator.setEProton(std::any_cast<double>(machine["eProton"]));
        generator.setEMuon(std::any_cast<double>(machine["eMuon"]));
        generator.setQ2Min(std::any_cast<double>(fc_set["Q2Min"]));
        generator.setYMin(std::any_cast<double>(fc_set["yMin"]));
        generator.setYMax(std::any_cast<double>(fc_set["yMax"]));
        generator.setUseNeutralCurrentEn(std::any_cast<bool>(variation["nc"]));
        generator.setUseChargedCurrentEn(std::any_cast<bool>(variation["cc"]));
        generator.setUseAntimuonEn(std::any_cast<bool>(variation["anti"]));

        // Run
        generator.run();

        // Log
        std::cout << "Run " << run_number << " end." << std::endl;
        std::cout << "Runs Remaining to Complete: "
                  << (run_stats.total - run_stats.done) << std::endl;

        // Update Counters
        run_stats.ongoing--;
        run_stats.done++;

        return 0;
    };

    // GO BRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
    workers.Map(run_function, run_args);
}

void runDISFromLHE() {
    // Run Options
    // User should specify the datasets they want to generate here.
    int n_events = 1000000;
    std::string output_dir = "../out/";
    std::string output_ext = "_1M";

    std::vector<std::map<string, std::any>> requests = {
        {
            {"machine", "MuIC"},
            {"fc_set", "hera"},
            {"input_lhef", "/nvme/omiguelc/madgraph/out/nc_bj/Events/MuIC/unweighted_events.lhe.gz"},
            {"output_name", "muic_nc_bj"},
        },
        {
            {"machine", "MuIC"},
            {"fc_set", "hera"},
            {"input_lhef", "/nvme/omiguelc/madgraph/out/nc_cbj/Events/MuIC/unweighted_events.lhe.gz"},
            {"output_name", "muic_nc_cbj"},
        },
        {
            {"machine", "MuIC"},
            {"fc_set", "hera"},
            {"input_lhef", "/nvme/omiguelc/madgraph/out/nc_cj/Events/MuIC/unweighted_events.lhe.gz"},
            {"output_name", "muic_nc_cj"},
        },
    };

    // Create the pool of workers
    auto hardware_concurrency = std::thread::hardware_concurrency();

    std::cout << "Cores " << hardware_concurrency << " that wil go brrrr."
              << std::endl;

    ROOT::TProcessExecutor workers(hardware_concurrency);

    // Build run args
    int job_number = 0;

    std::vector<DISLHERunArgs> run_args;

    for (auto &request : requests) {
        string machine = std::any_cast<const char *>(request["machine"]);
        string fc_set = std::any_cast<const char *>(request["fc_set"]);
        string input_lhef = std::any_cast<const char *>(request["input_lhef"]);
        string output_name =
            std::any_cast<const char *>(request["output_name"]);

        run_args.push_back({job_number++, n_events, output_dir, output_ext,
                            machines[machine], fiducial_cut_sets[fc_set],
                            input_lhef, output_name});
    }

    std::cout << "Generating " << run_args.size() << " datasets." << std::endl;

    // Define job function
    RunStats run_stats;
    run_stats.total = run_args.size();
    run_stats.ongoing = 0;
    run_stats.done = 0;

    auto run_function = [&run_stats](DISLHERunArgs run_args) {
        // Update Counters
        run_stats.ongoing++;

        // Get parameters
        auto &run_number = run_args.run_number;
        auto &n_events = run_args.n_events;
        auto &output_dir = run_args.output_dir;
        auto &output_ext = run_args.output_ext;
        auto &machine = run_args.machine;
        auto &fc_set = run_args.fc_set;
        auto &input_lhef = run_args.input_lhef;
        auto &base_output_name = run_args.output_name;

        // Build output name
        std::string output_limit_name =
            std::any_cast<const char *>(fc_set["output"]);
        std::string output_name =
            base_output_name + "_" + output_limit_name + output_ext + ".root";

        // Log
        std::cout << "Run " << run_number << " begin." << std::endl;
        std::cout << "Runs Remaining To Process: "
                  << (run_stats.total - run_stats.done - run_stats.ongoing)
                  << " end." << std::endl;
        std::cout << "Run " << run_number << " Output: " << output_name
                  << std::endl;

        // Configure
        PythiaGenerator generator;
        generator.setOutputPath(output_dir + output_name);
        generator.setNEvents(n_events);
        generator.setLHEInputFile(input_lhef);
        generator.setEProton(std::any_cast<double>(machine["eProton"]));
        generator.setEMuon(std::any_cast<double>(machine["eMuon"]));
        generator.setQ2Min(std::any_cast<double>(fc_set["Q2Min"]));
        generator.setYMin(std::any_cast<double>(fc_set["yMin"]));
        generator.setYMax(std::any_cast<double>(fc_set["yMax"]));

        // Run
        generator.run();

        // Log
        std::cout << "Run " << run_number << " end." << std::endl;
        std::cout << "Runs Remaining to Complete: "
                  << (run_stats.total - run_stats.done) << std::endl;

        // Update Counters
        run_stats.ongoing--;
        run_stats.done++;

        return 0;
    };

    // GO BRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
    workers.Map(run_function, run_args);
}
