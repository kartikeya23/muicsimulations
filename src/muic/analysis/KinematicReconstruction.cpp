//
// Created by omiguelc on 13/01/22.
//

#include "muic/analysis/KinematicReconstruction.h"

using namespace Pythia8;

KinematicReconstruction::KinematicReconstruction() {
    n = 0;
    sumE = 0;
    sumPx = 0;
    sumPy = 0;
    sumPz = 0;
    sumEPz = 0;
}

int KinematicReconstruction::getN() const { return n; }
double KinematicReconstruction::getSumE() const { return sumE; }
double KinematicReconstruction::getSumPx() const { return sumPx; }
double KinematicReconstruction::getSumPy() const { return sumPy; }
double KinematicReconstruction::getSumPz() const { return sumPz; }

void KinematicReconstruction::addParticle(const double &pe, const double &ppx,
                                          const double &ppy,
                                          const double &ppz) {
    n += 1;
    sumE += pe;
    sumPx += ppx;
    sumPy += ppy;
    sumPz += ppz;
    sumPT += sqrt(pow(ppx, 2) + pow(ppy, 2));
    sumEPz += pe - ppz;
}

std::tuple<double, double, double, double, double, double, double, double,
           double, double>
KinematicReconstruction::reconstruct(const Vec4 &pmuIn,
                                     const double &muThetaSmear,
                                     const double &muESmear,
                                     const double &s) const {
    // SHORT-CIRCUIT
    if (n == 0) {
        return {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
    }

    // RECONSTRUCT
    double sumPt2 = pow(sumPx, 2) + pow(sumPy, 2);
    double sumEPz2 = pow(sumEPz, 2);

    double yjb = sumEPz / (2.0 * pmuIn.e());
    double Q2jb = sumPt2 / (1 - yjb);
    double xjb = Q2jb / (s * yjb);

    double agljb = acos((sumPt2 - sumEPz2) / (sumPt2 + sumEPz2));

    double Q2DA = 4. * pow(pmuIn.e(), 2) * sin(agljb) *
                  (cos(muThetaSmear) + 1) /
                  (sin(agljb) + sin(muThetaSmear) - sin(agljb + muThetaSmear));
    double yDA = sin(muThetaSmear) * (1 - cos(agljb)) /
                 (sin(agljb) + sin(muThetaSmear) - sin(agljb + muThetaSmear));
    double xDA = Q2DA / (s * yDA);

    double yA4 = (muESmear - pmuIn.e()) /
                 (muESmear + sqrt(sumPt2) / sin(agljb) - 2.0 * pmuIn.e());
    double Q2A4 = 4. * pow(pmuIn.e(), 2) *
                      (pmuIn.e() - sqrt(sumPt2) / sin(agljb)) /
                      (muESmear + sqrt(sumPt2) / sin(agljb) - 2.0 * pmuIn.e()) +
                  4. * pmuIn.e() * muESmear;
    double xA4 = Q2A4 / (s * yA4);

    return {agljb, Q2jb, xjb, yjb, Q2DA, xDA, yDA, Q2A4, xA4, yA4};
}
